package query

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/tahasevim/zeo-task/auth"
	"github.com/tahasevim/zeo-task/config"
)

func Task1Handler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"$a": "The Lord of the Rings: The Fellowship of the Ring",
	}
	resp := sendQuery(config.Task1Query, params, false)
	w.Write(resp)
}

func Task2Handler(w http.ResponseWriter, r *http.Request) {
	resp := sendQuery(config.Task2Query, nil, true)
	w.Write(resp)
}

func Task3Handler(w http.ResponseWriter, r *http.Request) {
	session, err := auth.Store.Get(r, "auth-session")
	if err != nil {
		log.Fatal(err)
	}
	metadata := session.Values["metadata"].(map[string]interface{})
	params := map[string]string{
		"$a": metadata["name"].(string) + " " + metadata["surname"].(string),
	}
	resp := sendQuery(config.Task3Query, params, false)
	w.Write(resp)
}

func Task4Handler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"$a": "Peter Jackson",
	}
	resp := sendQuery(config.Task4Query, params, false)
	w.Write(resp)
}

func Task5Handler(w http.ResponseWriter, r *http.Request) {
	stemResp, err := http.Get("http://" + config.StemmAppAddress)
	if err != nil {
		log.Println(err)
	}
	defer stemResp.Body.Close()
	b, err := ioutil.ReadAll(stemResp.Body)
	if err != nil {
		log.Println(err)
	}
	params := map[string]string{
		"$a": string(b),
	}
	resp := sendQuery(config.Task5Query, params, false)
	w.Write(resp)
}

func Task6Handler(w http.ResponseWriter, r *http.Request) {
	resp := sendQuery(config.Task6Query, nil, false)
	w.Write(resp)
}

func Task7Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		genreResp := sendQuery("{q(func: has(~genre)) {name@.}}", nil, false)
		data := struct {
			Genre []string
		}{
			Genre: task7Utility(genreResp),
		}
		tmpl := template.Must(template.ParseFiles("templates/genre_auto.html"))
		tmpl.Execute(w, data)
		return
	}
	r.ParseForm()
	params := map[string]string{
		"$a": r.Form.Get("genre_name"),
	}
	resp := sendQuery(config.Task7Query, params, false)
	w.Write(resp)
}

func Task8Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		tmpl := template.Must(template.ParseFiles("templates/query_builder.html"))
		tmpl.Execute(w, nil)
		return
	}
	r.ParseForm()
	log.Println(r.Form)
	params := map[string]string{
		"$genre_name":        r.Form.Get("genre_name"),
		"$director_name":     r.Form.Get("director_name"),
		"$release_year_from": r.Form.Get("release_year_from"),
		"$release_year_to":   r.Form.Get("release_year_to"),
		"$actor_name":        r.Form.Get("actor_name"),
	}
	resp := sendQuery(config.Task8Query, params, false)
	w.Write(resp)
}
