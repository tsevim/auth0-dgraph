package query

import (
	"context"
	"log"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"github.com/tahasevim/zeo-task/config"
	"google.golang.org/grpc"
)

var (
	dgraphClient *dgo.Dgraph
)

func Init() {
	dgraphClient = getClient(config.DgraphServerAddress)
	setSchema(`
		director.film: uid @reverse .
		genre: uid @reverse .
		initial_release_date: dateTime @index(year) .
		name: string @index(term, trigram, fulltext) @lang .
	`)
}

func setSchema(schema string) {
	op := &api.Operation{
		Schema: schema,
	}
	err := dgraphClient.Alter(context.Background(), op)
	if err != nil {
		log.Fatal(err)
	}
}

func getClient(dgraphAddr string) *dgo.Dgraph {
	conn, err := grpc.Dial(dgraphAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return dgo.NewDgraphClient(api.NewDgraphClient(conn))
}

func sendQuery(query string, queryParams map[string]string, isConversion bool) []byte {
	var resp *api.Response
	var err error
	switch {
	case queryParams != nil:
		resp, err = dgraphClient.NewReadOnlyTxn().QueryWithVars(context.Background(), query, queryParams)
	case isConversion:
		setSchema("initial_release_date: int  @index(int).")
		resp, err = dgraphClient.NewReadOnlyTxn().Query(context.Background(), query)
		setSchema("initial_release_date: dateTime @index(year).")
	default:
		resp, err = dgraphClient.NewReadOnlyTxn().Query(context.Background(), query)
	}
	if err != nil {
		log.Fatal(err)
	}
	return getPrettyJson(resp.Json, isConversion)
}
