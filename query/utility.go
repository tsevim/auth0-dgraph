package query

import (
	"encoding/json"
	"log"
	"time"
)

func getPrettyJson(jsonResp []byte, isConversion bool) []byte {
	var raw map[string]interface{}
	json.Unmarshal(jsonResp, &raw)
	if isConversion {
		raw = task2Utility(raw)
	}

	jsonPrettyResp, err := json.MarshalIndent(raw, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	return jsonPrettyResp
}

func task2Utility(raw map[string]interface{}) map[string]interface{} {
	for i, val := range raw["byGenre"].([]interface{}) {
		epochVals := val.(map[string]interface{})
		timeVals := map[string]interface{}{
			"genre_name": epochVals["genre_name"],
			"minimum":    time.Unix(int64(epochVals["minimum"].(float64)), 0),
			"maximum":    time.Unix(int64(epochVals["maximum"].(float64)), 0),
			"average":    time.Unix(int64(epochVals["average"].(float64)), 0),
		}
		raw["byGenre"].([]interface{})[i] = timeVals
	}
	return raw
}

func task7Utility(genreJson []byte) []string {
	var respTemplate map[string][]map[string]string
	genreList := []string{}
	if err := json.Unmarshal(genreJson, &respTemplate); err != nil {
		log.Panic(err)
	}
	for _, tempMap := range respTemplate["q"] {
		genreList = append(genreList, tempMap["name@."])
	}
	return genreList
}
