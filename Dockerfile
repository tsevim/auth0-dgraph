FROM golang:1.12

WORKDIR $GOPATH/src/github.com/tahasevim/zeo-task
COPY . .

EXPOSE 5000

RUN go get github.com/gorilla/mux && \
    go get github.com/gorilla/sessions && \
    go get github.com/urfave/negroni && \
    go get golang.org/x/oauth2 && \
	go get github.com/dgraph-io/dgo

 
CMD ["go", "run", "main.go"]
