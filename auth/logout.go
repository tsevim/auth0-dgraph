package auth

import (
	"net/http"
	"net/url"

	"github.com/tahasevim/zeo-task/config"
)

func LogoutHandler(w http.ResponseWriter, r *http.Request) {

	var Url *url.URL
	Url, err := url.Parse("https://" + config.Domain)

	if err != nil {
		panic("boom")
	}

	Url.Path += "/v2/logout"
	parameters := url.Values{}
	parameters.Add("returnTo", "http://"+config.Address+"/login")
	parameters.Add("client_id", config.ClientID)
	Url.RawQuery = parameters.Encode()
	http.Redirect(w, r, Url.String(), http.StatusTemporaryRedirect)
}
