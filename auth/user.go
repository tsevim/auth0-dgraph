package auth

import (
	"encoding/json"
	"log"
	"net/http"
)

func UserHandler(w http.ResponseWriter, r *http.Request) {
	session, err := Store.Get(r, "auth-session")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	metadata := session.Values["metadata"]
	b, err := json.MarshalIndent(metadata, " ", " ")
	if err != nil {
		log.Fatal(err)
	}
	w.Write(b)
}
