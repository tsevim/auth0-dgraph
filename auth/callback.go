package auth

import (
	"context"
	_ "crypto/sha512"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/tahasevim/zeo-task/config"
	"golang.org/x/oauth2"
)

func CallbackHandler(w http.ResponseWriter, r *http.Request) {

	conf := &oauth2.Config{
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
		RedirectURL:  "http://" + config.Address + "/callback",
		Scopes:       []string{"openid", "profile", "user_metadata"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://" + config.Domain + "/authorize",
			TokenURL: "https://" + config.Domain + "/oauth/token",
		},
	}

	state := r.URL.Query().Get("state")
	session, err := Store.Get(r, "state")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if state != session.Values["state"] {
		http.Error(w, "Invalid state parameter", http.StatusInternalServerError)
		return
	}
	code := r.URL.Query().Get("code")

	token, err := conf.Exchange(context.TODO(), code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Getting now the userInfo
	client := conf.Client(context.TODO(), token)
	resp, err := client.Get("https://" + config.Domain + "/userinfo")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()
	var profile map[string]interface{}
	if err = json.NewDecoder(resp.Body).Decode(&profile); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	session, err = Store.Get(r, "auth-session")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["id_token"] = token.Extra("id_token")
	session.Values["access_token"] = token.AccessToken
	session.Values["profile"] = profile
	if _, ok := profile["family_name"].(string); ok {
		session.Values["metadata"] = map[string]interface{}{
			"name":    strings.Split(profile["name"].(string), " ")[0],
			"surname": profile["family_name"].(string),
			"email":   profile["nickname"].(string) + "@gmail.com",
		}
	} else {
		temp := profile["https://"+config.Address+"/user_metadata"].(map[string]interface{})
		session.Values["metadata"] = map[string]interface{}{
			"name":    temp["name"].(string),
			"surname": temp["surname"].(string),
			"email":   profile["nickname"].(string) + "@gmail.com",
		}
	}
	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Redirect to logged in page
	http.Redirect(w, r, "/", http.StatusSeeOther)

}
