import nltk

from flask import Flask
from nltk.tokenize import RegexpTokenizer
from snowballstemmer import TurkishStemmer

app = Flask(__name__)
nltk.download("punkt")

@app.route("/")
def stemming():
    return make_stemming(STEMMING_TEXT)

STEMMING_TEXT = """
	Ey Türk gençliği!
	Birinci vazifen, Türk istiklâlini, Türk Cumhuriyetini, ilelebet, muhafaza ve müdafaa etmektir. 
	Mevcudiyetinin ve istikbalinin yegâne temeli budur. Bu temel, senin, en kıymetli hazinendir. 
	İstikbalde dahi, seni, bu hazineden, mahrum etmek isteyecek, dahilî ve haricî, bedhahların olacaktır.
	Bir gün, istiklâl ve cumhuriyeti müdafaa mecburiyetine düşersen, vazifeye atılmak için, içinde bulunacağın vaziyetin imkân ve şerâitini düşünmeyeceksin!
	Bu imkân ve şerâit, çok nâmüsait bir mahiyette tezahür edebilir. 
	İstiklâl ve cumhuriyetine kastedecek düşmanlar, bütün dünyada emsali görülmemiş bir galibiyetin mümessili olabilirler.
	Cebren ve hile ile aziz vatanın, bütün kaleleri zaptedilmiş, bütün tersanelerine girilmiş, bütün orduları dağıtılmış ve memleketin her köşesi bilfiil işgal edilmiş olabilir.
	Bütün bu şerâitten daha elîm ve daha vahim olmak üzere, memleketin dahilinde, iktidara sahip olanlar gaflet ve dalâlet ve hattâ hıyanet içinde bulunabilirler.
	Hattâ bu iktidar sahipleri şahsî menfaatlerini, müstevlilerin siyasi emelleriyle tevhit edebilirler. 
	Millet, fakr-u-zaruret içinde harap ve bîtap düşmüş olabilir.
	Ey Türk istikbalinin evlâdı! İşte, bu ahval ve şerâit içinde dahi, vazifen; Türk İstiklâl ve cumhuriyetini kurtarmaktır!
	Muhtaç olduğun kudret, damarlarındaki asîl kanda, mevcuttur!
"""

def make_stemming(text):
    tr = TurkishStemmer()
    tokenizer = RegexpTokenizer(r"\w+")
    stemmed_text = " ".join([tr.stemWord(word).lower() for word in tokenizer.tokenize(text)])
    return stemmed_text

if  __name__ == "__main__":
    app.run(host="0.0.0.0", port=4000)
