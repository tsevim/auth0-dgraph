# Zeo Agency Task with DGraph and Auth0

## Before Start  
 You need Docker and Docker Compose to execute project.  
  - Download [Docker](https://docs.docker.com/)   
  - Download [Docker Compose](https://docs.docker.com/compose/install/)  
  
You need to configure Auth0 rule to let Auth0 know what it will obey. Also if you don't want to run locally but in a remote server, please change environment variable `HOST_ADDR` that is placed inside the `docker-compose.yml` file. 

Since you may desire to create your own Auth0 account and add this application to your account, please make sure that other environment variables in `docker-compose.yml` file are set properly. Do not forget to change URL's such as `Allowed Callback URLs` and `Application Logout URLs` in application settings that you created with your own Auth0 account.  
### Example Auth0 Rule
```javascript
function (user, context, callback) {
  //var namespace = 'https://localhost:5000/'; // IF WORKING LOCALLY
  var namespace = 'https://<HOST_IP>:5000/'; // IF WORKING IN REMOTE SERVER
  context.idToken[namespace + 'user_metadata'] = user.user_metadata;
  context.accessToken[namespace + 'app_metadata'] = user.app_metadata;
  callback(null, user, context);
}
```
### Example HOST_ADDR Environment Variable Inside docker-compose.yml
If working locally  
`HOST_ADDR=localhost:5000`  
If working in a remote server  
`HOST_ADDR=<HOST_IP>:5000` 

Final prequisite is preparing sample data for DGraph. Create a directory with `mkdir` and change working directory using `cd` command utility.   
`$ mkdir ~/dgraph`  
`$ cd ~/dgraph`

Then download sample data with `wget`  
`$ wget "https://github.com/dgraph-io/tutorial/blob/master/resources/1million.rdf.gz?raw=true" -O 1million.rdf.gz -q`  

**Note:** You may required to install `wget` if you didn't install before. You can install `wget`utility with your package manager such as `apt` or `homebrew`  
## Installation and Execution
Install repo using `git clone`  
`$ git clone https://github.com/tahasevim/zeo-task.git `

Change working directory with `cd`  
`$ cd zeo-task`  

Then run below command to bootstrap services   
`$ docker-compose build && docker-compose up`  

Finally load sample data for the DGraph  
`$ ./load_data.sh`

Services are now up and ready to accomplish their tasks. You can check logs of container to see what is going on.  
**Notes**  
- You should load data after all services are up.  
- If you want to use Ratel UI, make sure that correct DGraph server url is given especially correct port is given. Port should be 8080.  
- While registering with Auth0, please use real actor name or surname to see results of Task 3
## Resources
  - [Auth0 with Go](https://auth0.com/docs/quickstart/webapp/golang/01-login) to implement Auth0
  - [DGraph Tutorial](https://tour.dgraph.io/) to learn basics of DGraph 
  - [dgo](https://github.com/dgraph-io/dgo) to built basic Go client for the DGraph server  
  - [Autocomplete](https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_autocomplete) for primitive autocomplete with Javascript
