package config

import "os"

var (
	Address             = os.Getenv("HOST_ADDR")
	WebappAddress       = os.Getenv("WEB_APP_ADDR")
	DgraphServerAddress = os.Getenv("DGRAPH_SERVER_ADDR")
	StemmAppAddress     = os.Getenv("STEMM_APP_ADDR")
	Domain              = os.Getenv("DOMAIN")
	ClientID            = os.Getenv("CLIENT_ID")
	ClientSecret        = os.Getenv("CLIENT_SECRET")
)

const (
	Task1Query = ` query q($a: string){
		q(func: allofterms(name@., $a)){
			name@.
			initial_release_date
			~director.film{
				name@.
				expand(_all_){
					name@.
					initial_release_date
					genre{
						name@.
					}
				}
			}
			starring{
				performance.actor{
					name@.		
				}
				performance.character{
					name@.
				}
			}
		}
	}
	`
	Task2Query = `
	{
		var(func: has(genre)) @groupby(genre){
			mn as min(initial_release_date)
			mx as max(initial_release_date)
			total as sum(initial_release_date)
			c as count(uid)
		}
		
		byGenre(func: uid(mn)) {
			genre_name: name@.
		  	minimum: val(mn)
			maximum: val(mx)
			average: math(total/c)
		}
	}
	`
	Task3Query = ` query q($a: string)
	{
		q(func: anyofterms(name@., $a)) @filter(has(actor.film))@normalize{
			actor_name : name@.
			actor.film{
				performance.film{
					film_name: name@.
				}
			  	performance.character{
					characte: name@.
				}
			}
		}
	}
	`
	j
	Task4Query = ` query var($a: string)
	{
		var(func: allofterms(name@., $a))@cascade{
			director.film {
				starring @groupby(performance.actor){
					c as count(uid)
				}
		  }
		}
		  
		freq(func: uid(c), orderdesc:val(c), first:10){
			actor_name: name@.
			frequency: val(c)
		}
	}
	`

	Task5Query = `query q($a: string)
	{
		q(func: anyoftext(name@., $a)) @filter(has(initial_release_date)) {
			name@.
		}
	}
	`

	Task6Query = `
	{
		films as var(func: has(initial_release_date)) @filter(regexp(name@en, /.*man.*/i) and not regexp(name@en, /.*woman.*/i)){
			starring @groupby(performance.actor){
				actor_num as count(uid)
			}
		}
		
		byActor(func: uid(actor_num), orderdesc:val(actor_num), first:15) @cascade{
			actor_name: name@.
			actor.film {
				performance.film @filter(not uid(films)){
					film_name: name@.			
				}
			}
		}
	}
	`
	Task7Query = ` query q($a: string)
	{
		q(func: allofterms(name@., $a)) @normalize @cascade{
			genre_name: name@.
			~genre @filter(lt(initial_release_date, 2013)){
				film_name: name@.
				release_date: initial_release_date
		  	}
	  	}
	}
	`
	Task8Query = ` 
		query q($genre_name : string, $director_name:string, 
			$release_year_from:int, $release_year_to:int, $actor_name:string)
	{
		var(func: allofterms(name@., $director_name)) @cascade{
			films as director.film @filter(ge(initial_release_date, $release_year_from) 
										and le(initial_release_date, $release_year_to)){
				genre @filter(allofterms(name@. , $genre_name))
				starring {
					performance.actor @filter(allofterms(name@., $actor_name))
				}
			}
		}
		  
		q(func: uid(films)){
			film_name: name@.
			genre {
				genre_name: name@.
			}
			initial_release_date
			starring{
				performance.actor{
					actor_name: name@.		
				}
				performance.character{
					character: name@.
				}
			}
		}
	}
	`
)
