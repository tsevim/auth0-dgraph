package main

import (
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/tahasevim/zeo-task/auth"
	"github.com/tahasevim/zeo-task/config"
	"github.com/tahasevim/zeo-task/query"
	"github.com/urfave/negroni"
)

type TaskRouter struct {
	routeMap map[string]func(http.ResponseWriter, *http.Request)
}

func main() {
	auth.Init()
	query.Init()
	r := mux.NewRouter()
	taskRouter := newTaskRouter()
	r.Handle("/task/{taskNum}", negroni.New(
		negroni.HandlerFunc(auth.IsAuthenticated),
		negroni.Wrap(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			taskNum := mux.Vars(r)["taskNum"]
			handler := taskRouter.routeMap[taskNum]
			handler(w, r)
		})),
	))

	r.HandleFunc("/login", auth.LoginHandler)
	r.HandleFunc("/logout", auth.LogoutHandler)
	r.HandleFunc("/callback", auth.CallbackHandler)
	r.Handle("/", negroni.New(
		negroni.HandlerFunc(auth.IsAuthenticated),
		negroni.Wrap(http.HandlerFunc(homeHandler)),
	))
	r.Handle("/user", negroni.New(
		negroni.HandlerFunc(auth.IsAuthenticated),
		negroni.Wrap(http.HandlerFunc(auth.UserHandler)),
	))
	r.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("public/"))))

	http.Handle("/", r)
	log.Println("Server listening on http://" + config.Address)
	log.Println(http.ListenAndServe(config.WebappAddress, nil))
}

func newTaskRouter() *TaskRouter {
	return &TaskRouter{
		routeMap: map[string]func(http.ResponseWriter, *http.Request){
			"1": query.Task1Handler,
			"2": query.Task2Handler,
			"3": query.Task3Handler,
			"4": query.Task4Handler,
			"5": query.Task5Handler,
			"6": query.Task6Handler,
			"7": query.Task7Handler,
			"8": query.Task8Handler,
		},
	}
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("templates/home.html"))
	session, err := auth.Store.Get(r, "auth-session")
	if err != nil {
		log.Fatal(err)
	}
	metadata := session.Values["metadata"]
	tmpl.Execute(w, metadata)
}
